---
subtitle: What even is this
tags: []
title: First post from me
date: 2018-05-16 16:10:41 +0200

---
## **GitLab Runner**

To perform the actual build, you need to install GitLab Runner which is written in Go.

It can run on any platform for which you can build Go binaries, including Linux, OSX, Windows, FreeBSD and Docker.

It can test any programming language including .Net, Java, Python, C, PHP and others.

GitLab Runner has [many features](https://docs.gitlab.com/runner#features), including [autoscaling](https://docs.gitlab.com/runner/configuration/autoscale.html), [great Docker support](https://docs.gitlab.com/runner/executors/docker.html), and the ability to run multiple jobs concurrently.

### [Install GitLab Runner](https://docs.gitlab.com/runner/install)