---
title: "Россия"
date: 2018-05-23 16:20:45 +0200
subtitle: "Российская Федерация"
tags:
- bigimg
- example
bigimg:
- src: "/uploads/2018/05/23/900px-Flag_of_Russia.svg.png"
  desc: "Росси́я"

---
**Росси́я** (от [греч.](https://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B5%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D1%8F%D0%B7%D1%8B%D0%BA "Греческий язык") Ρωσία — [Русь](https://ru.wikipedia.org/wiki/%D0%A0%D1%83%D1%81%D1%8C "Русь"); официально **Росси́йская Федера́ция**, на практике также используется [аббревиатура](https://ru.wikipedia.org/wiki/%D0%90%D0%B1%D0%B1%D1%80%D0%B5%D0%B2%D0%B8%D0%B0%D1%82%D1%83%D1%80%D0%B0 "Аббревиатура") **РФ**) — [суверенное](https://ru.wikipedia.org/wiki/%D0%A1%D1%83%D0%B2%D0%B5%D1%80%D0%B5%D0%BD%D0%BD%D0%BE%D0%B5_%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%BE "Суверенное государство") [государство](https://ru.wikipedia.org/wiki/%D0%93%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%BE "Государство") в [Восточной Европе](https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B0%D1%8F_%D0%95%D0%B2%D1%80%D0%BE%D0%BF%D0%B0 "Восточная Европа") и [Северной Азии](https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D0%B2%D0%B5%D1%80%D0%BD%D0%B0%D1%8F_%D0%90%D0%B7%D0%B8%D1%8F "Северная Азия"). Население: 146 880 432 чел. ([2018](https://ru.wikipedia.org/wiki/2018 "2018")). Территория России, определяемая её [Конституцией](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BD%D1%81%D1%82%D0%B8%D1%82%D1%83%D1%86%D0%B8%D1%8F_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%BE%D0%B9_%D0%A4%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8 "Конституция Российской Федерации"), составляет 17 125 191 км². Занимает [первое место в мире по территории](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2_%D0%B8_%D0%B7%D0%B0%D0%B2%D0%B8%D1%81%D0%B8%D0%BC%D1%8B%D1%85_%D1%82%D0%B5%D1%80%D1%80%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B9_%D0%BF%D0%BE_%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D0%B8 "Список государств и зависимых территорий по площади"), [шестое — по объёму ВВП по ППС](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%81%D1%82%D1%80%D0%B0%D0%BD_%D0%BF%D0%BE_%D0%92%D0%92%D0%9F_(%D0%9F%D0%9F%D0%A1) "Список стран по ВВП (ППС)") и [девятое — по численности населения](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%81%D1%82%D1%80%D0%B0%D0%BD_%D0%BF%D0%BE_%D0%BD%D0%B0%D1%81%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8E "Список стран по населению").

{{% alert warning %}}Предупреждение! Хорошая песня впереди!{{% /alert %}}

{{< youtube Ut_4hZBp51U >}}