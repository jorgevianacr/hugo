---
subtitle: Hello Dim!!
bigimg:
- src: "/uploads/2018/05/18/visual.jpg"
  desc: BMW
tags:
- example
- bigimg
title: Hello
date: 2018-05-17 15:26:05 +0200

---
## Just testing markdown and shortcodes

---

#### Chapter 1 <a id="chapter-1"></a>
  Content for chapter one.

___

{{< tweet 25172668038778881 >}}

___

{{< instagram BftSlPHl68J hidecaption >}}

___

{{< youtube Ut_4hZBp51U >}}

___

_Trying to create a shortcode for color text_

{{% textcolor "lime" %}}
If the shortcode worked when spelling out the color, then this sentence should be green
{{% /textcolor %}}

{{% textcolor "#3265fe" %}}
If the shortcode worked when passing the color code, then this sentence should be **FleetBack** blue
{{% /textcolor %}}

{{% alert info %}}As you can see, you ~~should~~ can use _markdown_ inside **shortcodes**{{% /alert %}}

___

_Go to_ **this** ~~article~~ POST:\

[New Front Matter]({{< ref "post/new-front-matter.md" >}})

***

Cats
: Rulers of the internet

___

> "A quote by me" - Me

___

##### List of things I hate:
1. _Lists_
1. _Things_
1. _Irony_

***

+ some
  - nested
  - bullet
  - points
+ in
+ markdown

___

### Table of Contents
  * [Chapter 1](#chapter-1)
  * [Chapter 2](#chapter-2)

---

#### Chapter 2 <a id="chapter-2"></a>
  Content for chapter two.

***

### Volleyball markdown table

| Volleyball positions | Role |
| ------ | ----------- |
| Wing spiker | Attacking |
| Setter | Attacking/Creative |
| Libero | Defensive |


---

### Volleyball markdown table (_uglier markdown still works_)

Volleyball positions | Role
:---: | :---:
Opposite hitter | Attacking/Blocking
Middle blocker | Attacking/Blocking
Defensive specialist | Defensive

___

{{% alert warning %}}Warning! Cute cat ahead !{{% /alert %}}

![Cato](https://media.giphy.com/media/vFKqnCdLPNOKc/giphy.gif)

___

**Image**

_Maybe I can create a shortcode_

{{% picture "https://f4.bcbits.com/img/a4047854946_10.jpg" 300 300 "Original was 750x750" %}}

___
