---
title: New Front Matter
date: 2018-05-18 14:52:14 +0200
bigimg:
- src: "/uploads/2018/05/18/visual.jpg"
  desc: BMW
subtitle: Hopefully this works
tags:
- example
- bigimg

---
## What are Archetypes?

**Archetypes** are content template files in the [archetypes directory](https://gohugo.io/getting-started/directory-structure/) of your project that contain preconfigured [front matter](https://gohugo.io/content-management/front-matter/) and possibly also a content disposition for your website’s [content types](https://gohugo.io/content-management/types/). These will be used when you run `hugo new`.

The `hugo new` uses the `content-section` to find the most suitable archetype template in your project. If your project does not contain any archetype files, it will also look in the theme.
